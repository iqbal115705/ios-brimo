package pfm
import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject

import com.kms.katalon.core.annotation.Keyword
import com.kms.katalon.core.checkpoint.Checkpoint
import com.kms.katalon.core.checkpoint.CheckpointFactory
import com.kms.katalon.core.keyword.builtin.CallTestCaseKeyword
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling
import com.kms.katalon.core.testcase.TestCase
import com.kms.katalon.core.testcase.TestCaseFactory
import com.kms.katalon.core.testdata.TestData
import com.kms.katalon.core.testdata.TestDataFactory
import com.kms.katalon.core.testobject.ObjectRepository
import com.kms.katalon.core.testobject.TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI

import internal.GlobalVariable

import org.openqa.selenium.WebElement
import org.openqa.selenium.WebDriver
import org.openqa.selenium.By

import com.kms.katalon.core.mobile.keyword.internal.MobileDriverFactory
import com.kms.katalon.core.webui.driver.DriverFactory

import com.kms.katalon.core.testobject.RequestObject
import com.kms.katalon.core.testobject.ResponseObject
import com.kms.katalon.core.testobject.ConditionType
import com.kms.katalon.core.testobject.TestObjectProperty

import com.kms.katalon.core.mobile.helper.MobileElementCommonHelper
import com.kms.katalon.core.util.KeywordUtil

import com.kms.katalon.core.webui.exception.WebElementNotFoundException

import cucumber.api.java.en.And
import cucumber.api.java.en.Given
import cucumber.api.java.en.Then
import cucumber.api.java.en.When



class PFM {
	/**
	 * The step definitions below match with Katalon sample Gherkin steps
	 */

	@When("I go to PFM from fast menu")
	def I_go_to_PFM_from_fast_menu() {
		Mobile.callTestCase(findTestCase('PFM/go to PFM from Fast Menu'), [:], FailureHandling.STOP_ON_FAILURE)
	}

	@And('I go to (.*) page')
	def I_go_to_some_page(String page) {
		Mobile.callTestCase(findTestCase('PFM/go to page'), [('page'): page], FailureHandling.STOP_ON_FAILURE)
	}

	@Then('I verify the (.*) page there is no record yet')
	def i_verify_the_page_is_empty(String page) {
		Mobile.callTestCase(findTestCase('PFM/service PFM empty'), [('page'): page], FailureHandling.STOP_ON_FAILURE)
	}

	@And('I verify the (.*) range in laporan page')
	def I_verify_the_range_in_laporan_page(String range) {
		Mobile.callTestCase(findTestCase('PFM/service range laporan'), [('range'):range], FailureHandling.STOP_ON_FAILURE)
	}

	@When('I go to tambah catatan for (.*)')
	def i_go_to_tambah_catatan(String page) {
		Mobile.callTestCase(findTestCase('PFM/go to buat catatan'), [('page'): page], FailureHandling.STOP_ON_FAILURE)
	}

	@And('I try input to verify (.*)')
	def I_try_input_to_verify_amount(String amount) {
		Mobile.callTestCase(findTestCase('PFM/input invalid amount'), [('amount'): amount], FailureHandling.STOP_ON_FAILURE)
	}

	@And('I try choose (.*) date')
	def I_try_choose_date(String date) {
		Mobile.callTestCase(findTestCase('PFM/service input date'), [('date'): date], FailureHandling.STOP_ON_FAILURE)
	}

	@And('I input jumlah (.*)')
	def I_input_jumlah_amount(String amount) {
		Mobile.callTestCase(findTestCase('PFM/input valid amount'), [('amount'): amount], FailureHandling.STOP_ON_FAILURE)
	}

	@And('I choose for (.*)')
	def I_choose_category(String category) {
		Mobile.callTestCase(findTestCase('PFM/service pilih kategori'), [('category'): category], FailureHandling.STOP_ON_FAILURE)
	}

	@And('I choose pembayaran for (.*)')
	def I_choose_pembayaran_payment(String payment) {
		Mobile.callTestCase(findTestCase('PFM/service pilih pembayaran'), [('payment'): payment], FailureHandling.STOP_ON_FAILURE)
	}

	@And('I input note (.*)')
	def I_input_note(String note) {
		Mobile.callTestCase(findTestCase('PFM/service input note'), [('note'): note], FailureHandling.STOP_ON_FAILURE)
	}

	@And('I tap simpan button')
	def I_tap_simpan() {
		Mobile.callTestCase(findTestCase('PFM/service simpan catatan'), [:], FailureHandling.STOP_ON_FAILURE)
	}

	@Then('I verify the input (.*) is listed')
	def I_verify_the_input_is_listed(String category) {
		Mobile.callTestCase(findTestCase('PFM/service verify list catatan tersimpan'), [('category'):category], FailureHandling.STOP_ON_FAILURE)
	}

	@When('I verify the detail of spending which contain (.*), (.*), (.*), (.*) and (.*)')
	def I_verify_the_spendind_detail_contain(String category, String note, String amount, String date, String payment) {
		Mobile.callTestCase(findTestCase('PFM/service detail pengeluaran'), [('category'):category, ('note'):note, ('amount'):amount, ('date'):date, ('payment'):payment], FailureHandling.STOP_ON_FAILURE)
	}

	@And('I verify the spending is listed in laporan by (.*)')
	def I_verify_the_spending_is_listed_in_laporan_by_category(String category) {
		Mobile.callTestCase(findTestCase('PFM/service verify kategori page laporan'), [('category'):category], FailureHandling.STOP_ON_FAILURE)
	}

	@When('I check the list look for a reference number')
	def I_check_the_list_look_for_ref_num() {
		Mobile.callTestCase(findTestCase('PFM/service check ref num'), [:], FailureHandling.STOP_ON_FAILURE)
	}

	@And('I try edit list')
	def i_try_edit_list() {
		Mobile.tap(findTestObject('PFM/detail pengeluaran/XCUIElementTypeButton - Edit'), 0)
	}

	@And('I edit category for (.*)')
	def i_edit_category_for(String new_category) {
		Mobile.callTestCase(findTestCase('PFM/service edit kategori'), [('new_category') : new_category], FailureHandling.STOP_ON_FAILURE)
	}

	@And('I edit note for (.*)')
	def i_edit_note(String page) {
		Mobile.callTestCase(findTestCase('PFM/service edit note'), [('page'):page], FailureHandling.STOP_ON_FAILURE)
	}

	@When('I check the list look for have no reference number')
	def I_check_the_list_look_for_no_ref_num() {
		Mobile.callTestCase(findTestCase('PFM/service check no ref num'), [:], FailureHandling.STOP_ON_FAILURE)
	}

	@And('I edit jumlah (.*)')
	def I_edit_jumlah_amount(String amount) {
		Mobile.callTestCase(findTestCase('PFM/edit valid amount'), [('amount'): amount], FailureHandling.STOP_ON_FAILURE)
	}

	@And('I try edit (.*) date')
	def I_try_edit_date(String date) {
		Mobile.callTestCase(findTestCase('PFM/service edit date'), [('date'): date], FailureHandling.STOP_ON_FAILURE)
	}

	@And('I edit pembayaran for (.*)')
	def I_edit_pembayaran_payment(String payment) {
		Mobile.callTestCase(findTestCase('PFM/service edit pembayaran'), [('payment'): payment], FailureHandling.STOP_ON_FAILURE)
	}

	@And('I try delete list for (.*)')
	def i_try_delete_list(String page) {
		Mobile.callTestCase(findTestCase('PFM/service delete list'), [('page'):page], FailureHandling.STOP_ON_FAILURE)
	}

	@And('I input pemasukan note (.*)')
	def I_input_note_for_pemasukan(String note) {
		Mobile.callTestCase(findTestCase('PFM/service input note pemasukan'), [('note'): note], FailureHandling.STOP_ON_FAILURE)
	}

	@When('I verify the detail of earning which contain (.*), (.*), (.*) and (.*)')
	def I_verify_the_earning_detail_contain(String category, String note, String amount, String date) {
		Mobile.callTestCase(findTestCase('PFM/service detail pemasukan'), [('category'):category, ('note'):note, ('amount'):amount, ('date'):date], FailureHandling.STOP_ON_FAILURE)
	}

	@And('I verify the earning is listed in laporan page')
	def I_verify_the_earning_is_listed_in_laporan_page() {
		Mobile.callTestCase(findTestCase('PFM/service verify pemasukan page laporan'), [:], FailureHandling.STOP_ON_FAILURE)
	}
}