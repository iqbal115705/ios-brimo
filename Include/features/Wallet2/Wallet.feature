#Author: your.email@your.domain.com
#Keywords Summary :
#Feature: List of scenarios.
#Scenario: Business rule through list of steps with arguments.
#Given: Some precondition step
#When: Some key actions
#Then: To observe outcomes or validation
#And,But: To enumerate more Given,When,Then steps
#Scenario Outline: List of steps for data-driven as an Examples and <placeholder>
#Examples: Container for s table
#Background: List of steps run before each of the scenarios
#""" (Doc Strings)
#| (Data Tables)
#@ (Tags/Labels):To group Scenarios
#<> (placeholder)
#""
## (Comments)
#Sample Feature Definition Template
Feature: Wallet
    
  @Invalid  
  Scenario Outline: User top up wallet - Abnormal Saldo
    #Given I start application
    #When I want login
    #When Abnormal Saldo - I try login with existing account <username> and <password>
    #Then I successfully go to dashboard
    #And I want to top up my wallet with <condition> for <username>
    #When I saw my top up wallet history
    When I want to add recipient of wallet
    When I try adding recipient of wallet in condition with <wallet>, <type> and <walletNumber>
    #Then Abnormal Saldo - I inputting <amount> for the wallet amount with <username>
#
    #Examples: 
      #| username   | password   | pin 	 | wallet			| type 			| walletNumber	| amount	| condition	|
      #| bribri0001 | Jakarta123 | 123457 | Gopay			| Customer	| 081212341234	| 10000		|	NEW				|
    
    
  #@Valid    
  #Scenario Outline: User top up wallet with CA
    #Given I start application
    #When I want login
    #When I try login with existing account <username> and <password>
    #Then I successfully go to dashboard
    #And I want to top up my wallet with <condition> for <username>
    #When I saw my top up wallet history
    #When I want to add recipient of wallet
    #When I try adding recipient of wallet in condition with <wallet>, <type> and <walletNumber>
    When I inputting <amount> for the wallet amount and <decision> with <name> , then choose account <debit>
    #When I confirm top up wallet with <detail> and <wallet>
    #When I validate my pin with <pin> before transaction
    #Then Transaction success
#
    #Examples: 
      #| username   | password   | pin 	 | wallet			| type 			| walletNumber	| amount	| detail			|	decision	| name		| debit	| condition	|
      #| bribri0001 | Jakarta123 | 123457 | Gopay			| Customer	| 081212341234	| 10000		| wallet			|	NULL			| NULL		| RATRI	|	NEW				|
      #| bribri0001 | Jakarta123 | 123457 | Gopay			| Driver		| 081212341234	| 10000		| wallet			| NULL			| NULL		| RATRI	|	NULL			|
      #| bribri0001 | Jakarta123 | 123457 | Gopay			| Merchant	| 081212341234	| 10000		| wallet			| NULL			| NULL		| RATRI	|	NULL			|
      #| bribri0001 | Jakarta123 | 123457 | LinkAja		| null			| 085691335269	| 10000		| wallet			| NULL			| NULL		| RATRI	|	NULL			|
      #| bribri0001 | Jakarta123 | 123457 | OVO				| null			| 080012345123	| 20000		| wallet			| NULL			| NULL		| RATRI	|	NULL			|
      #| bribri0001 | Jakarta123 | 123457 | ShopeePay	| null			| 081290825284	| 10000		| wallet			| NULL			| NULL		| RATRI	|	NULL			|
      #| bribri0001 | Jakarta123 | 123457 | DANA			| null			| 082299888040	| 10000		| wallet			| NULL			| NULL		| RATRI	|	NULL			|
      
   @Valid 
   Scenario Outline: User top up wallet with SA
    Given I start application
    When I want login
    When I try login with existing account <username> and <password>
    Then I successfully go to dashboard
    And I want to top up my wallet with <condition> for <username>
    When I saw my top up wallet history
    When I want to add recipient of wallet
    When I try adding recipient of wallet in condition with <wallet>, <type> and <walletNumber>
    When I inputting <amount> for the wallet amount and <decision> with <name> , then choose account <debit>
    When I confirm top up wallet with <detail> and <wallet>
    #When I validate my pin with <pin> before transaction
    #Then Transaction success

    Examples: 
      | username   | password   | pin 	 | wallet			| type 			| walletNumber	| amount	| detail			|	decision	| name		| debit					| condition	|
      | brimosv010 | Jakarta123 | 112233 | Gopay			| Customer	| 081212341234	| 10000		| wallet			|	Save			| Gopay 1	| Rihana Elaela	|	NULL			|
      #| bribri0001 | Jakarta123 | 123457 | Gopay			| Driver		| 081212341234	| 10000		| wallet			| NULL			| NULL		| Rihana Elaela	|	NULL			|
      #| bribri0001 | Jakarta123 | 123457 | Gopay			| Merchant	| 081212341234	| 10000		| wallet			| NULL			| NULL		| Rihana Elaela	|	NULL			|
      #| bribri0001 | Jakarta123 | 123457 | LinkAja		| null			| 085691335269	| 10000		| wallet			| NULL			| NULL		| Rihana Elaela	|	NULL			|
      #| bribri0001 | Jakarta123 | 123457 | OVO				| null			| 080012345123	| 20000		| wallet			| NULL			| NULL		| Rihana Elaela	|	NULL			|
      #| bribri0001 | Jakarta123 | 123457 | ShopeePay	| null			| 081290825284	| 10000		| wallet			| NULL			| NULL		| Rihana Elaela	|	NULL			|
      #| bribri0001 | Jakarta123 | 123457 | DANA			| null			| 082299888040	| 10000		| wallet			| NULL			| NULL		| Rihana Elaela	|	NULL			|
      #
      #
  #@Valid
#	Scenario Outline: User top up wallet from list
    #Given I start application
    #When I want login
    #When I try login with existing account <username> and <password>
    #Then I successfully go to dashboard
    #And I want to top up my wallet with <condition> for <username>
    #When I saw my top up wallet history
    #When I top up wallet from my list
    #When I inputting <amount> for the wallet amount and <decision> with <name> , then choose account <debit>
    #When I confirm top up wallet with <detail> and <wallet>
    #When I validate my pin with <pin> before transaction
    #Then Transaction success
#
    #Examples: 
      #| username   | password   | pin 	 | wallet			| amount	| detail			|	decision	| name		| debit	| condition	|
      #| bribri0001 | Jakarta123 | 123457 | Gopay			| 10000		| wallet			|	NULL			| NULL		| RATRI	|	NULL			|
      #
   @Valid   
   Scenario Outline: User top up wallet from history
    #Given I start application
    #When I want login
    #When I try login with existing account <username> and <password>
    #Then I successfully go to dashboard
    And I want to top up my wallet with <condition> for <username>
    When I saw my top up wallet history
    When I top up wallet from my history
    When I inputting <amount> for the wallet amount and <decision> with <name> , then choose account <debit>
    When I confirm top up wallet with <detail> and <wallet>
    #When I validate my pin with <pin> before transaction
    #Then Transaction success
#
    #Examples: 
      #| username   | password   | pin 	 | wallet			| amount	| detail			|	decision	| name		| debit	| condition	|
      #| bribri0001 | Jakarta123 | 123457 | Gopay			| 10000		| wallet			|	NULL			| NULL		| RATRI	|	NULL			|

	