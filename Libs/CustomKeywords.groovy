
/**
 * This class is generated automatically by Katalon Studio and should not be modified or deleted.
 */

import com.applitools.eyes.selenium.Eyes

import org.openqa.selenium.WebElement

import com.kms.katalon.core.testobject.TestObject

import java.lang.String

import com.applitools.eyes.RectangleSize



def static "com.kms.katalon.keyword.applitools.BasicKeywords.checkElement"(
    	Eyes eyes	
     , 	WebElement element	) {
    (new com.kms.katalon.keyword.applitools.BasicKeywords()).checkElement(
        	eyes
         , 	element)
}


def static "com.kms.katalon.keyword.applitools.BasicKeywords.checkTestObject"(
    	TestObject testObject	
     , 	String testName	) {
    (new com.kms.katalon.keyword.applitools.BasicKeywords()).checkTestObject(
        	testObject
         , 	testName)
}


def static "com.kms.katalon.keyword.applitools.BasicKeywords.checkWindow"(
    	String testName	) {
    (new com.kms.katalon.keyword.applitools.BasicKeywords()).checkWindow(
        	testName)
}


def static "common.choiceSumberDana.choiceSumberDanaWithName"(
    	String text	) {
    (new common.choiceSumberDana()).choiceSumberDanaWithName(
        	text)
}


def static "common.screenshot.takeScreenshotAsCheckpoint"() {
    (new common.screenshot()).takeScreenshotAsCheckpoint()
}

/**
	 * Open and return a connection to database
	 * @param dataFile absolute file path
	 * @return an instance of java.sql.Connection
	 */
def static "database.methods.connectDB"(
    	String url	
     , 	String dbname	
     , 	String port	
     , 	String username	
     , 	String password	) {
    (new database.methods()).connectDB(
        	url
         , 	dbname
         , 	port
         , 	username
         , 	password)
}

/**
	 * execute a SQL query on database
	 * @param queryString SQL query string
	 * @return a reference to returned data collection, an instance of java.sql.ResultSet
	 */
def static "database.methods.executeQuery"(
    	String queryString	) {
    (new database.methods()).executeQuery(
        	queryString)
}


def static "database.methods.executeUpdate"(
    	String queryString	) {
    (new database.methods()).executeUpdate(
        	queryString)
}


def static "database.methods.updateRefNum"() {
    (new database.methods()).updateRefNum()
}


def static "database.methods.closeDatabaseConnection"() {
    (new database.methods()).closeDatabaseConnection()
}

/**
	 * Execute non-query (usually INSERT/UPDATE/DELETE/COUNT/SUM...) on database
	 * @param queryString a SQL statement
	 * @return single value result of SQL statement
	 */
def static "database.methods.execute"(
    	String queryString	) {
    (new database.methods()).execute(
        	queryString)
}

/**
	 * Check if element present in timeout
	 * @param to Katalon test object
	 * @param timeout time to wait for element to show up
	 * @return true if element present, otherwise false
	 */
def static "screenshot.capture.Screenshot"() {
    (new screenshot.capture()).Screenshot()
}


def static "common.connection.offlineMode"() {
    (new common.connection()).offlineMode()
}


def static "common.connection.onlineMode"() {
    (new common.connection()).onlineMode()
}


def static "operation.features.clickNumber"(
    	String number	) {
    (new operation.features()).clickNumber(
        	number)
}


def static "operation.features.clickAmountPulsa"(
    	String amount	) {
    (new operation.features()).clickAmountPulsa(
        	amount)
}


def static "com.kms.katalon.keyword.applitools.EyesKeywords.eyesInit"() {
    (new com.kms.katalon.keyword.applitools.EyesKeywords()).eyesInit()
}


def static "com.kms.katalon.keyword.applitools.EyesKeywords.eyesOpen"(
    	String testName	
     , 	RectangleSize viewportSize	) {
    (new com.kms.katalon.keyword.applitools.EyesKeywords()).eyesOpen(
        	testName
         , 	viewportSize)
}


def static "com.kms.katalon.keyword.applitools.EyesKeywords.eyesClose"(
    	Eyes eyes	) {
    (new com.kms.katalon.keyword.applitools.EyesKeywords()).eyesClose(
        	eyes)
}


def static "com.kms.katalon.keyword.applitools.EyesKeywords.eyesOpenWithBaseline"(
    	String baselineName	
     , 	String testName	
     , 	RectangleSize viewportSize	) {
    (new com.kms.katalon.keyword.applitools.EyesKeywords()).eyesOpenWithBaseline(
        	baselineName
         , 	testName
         , 	viewportSize)
}
