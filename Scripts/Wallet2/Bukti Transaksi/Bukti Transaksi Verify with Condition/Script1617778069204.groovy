import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

import export.WriteTxt as WriteTxt

if (page_condition.toString() == 'normal') {
	
	Mobile.verifyElementExist(findTestObject('General/Bukti Transaksi/XCUIElementTypeStaticText - Transaksi Berhasil'), 0)
	
	Mobile.verifyElementExist(findTestObject('General/Bukti Transaksi/XCUIElementTypeStaticText - Jenis Transaksi'), 0)
	
	Mobile.verifyElementExist(findTestObject('General/Bukti Transaksi/XCUIElementTypeStaticText - Biaya Admin'), 0)
	
	tax = 'Rp0'
	
	switch (GlobalVariable.walletOption.toString().toLowerCase()) {
		case 'gopay':
			 if(GlobalVariable.typeGopay.toString().toLowerCase() == 'customer'){
				 tax = 'Rp1.000'
			 }
			 else if(GlobalVariable.typeGopay.toString().toLowerCase() == 'driver'){
				 tax = 'Rp0'
			 }
			 else{
				 tax = 'Rp0'
			 }
			 break
		case 'ovo':
		
			tax = 'Rp1.000'
				
			break
		case 'shopeepay':
			
			tax = 'Rp0'
			
			break
		case 'dana':
			
			tax = 'Rp0'
			
			break
		case 'linkaja':
			
			tax = 'Rp0'
			
			break
		default:
			break
	}
	
	text = Mobile.getText(findTestObject('General/Bukti Transaksi/XCUIElementTypeStaticText - Biaya Admin Nominal Dinamis'), 0)
	
	Mobile.verifyMatch(text, tax, true)
	
	dateTime = Mobile.getText(findTestObject('Object Repository/General/Bukti Transaksi/waktu dan tanggal'), 0)
	
	norek = GlobalVariable.accountNumber
	
	refnum = Mobile.getText(findTestObject('General/Bukti Transaksi/XCUIElementTypeStaticText - refNum'), 0)
	
	fitur = 'Top Up GoPay (iOS)'
	
	String file_name = '/Users/mochamadiqbal/Desktop/txt/'+ fitur +'.csv'
	WriteTxt data = new WriteTxt(file_name, true)
	data.WriteToTxt(dateTime, norek, refnum, fitur)
	
	CustomKeywords.'screenshot.capture.Screenshot'()
	
	Mobile.tap(findTestObject('General/Bukti Transaksi/XCUIElementTypeButton - OK'), 0)
	
	Mobile.delay(3)
	
	CustomKeywords.'screenshot.capture.Screenshot'()
	
}