import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

Mobile.verifyElementExist(findTestObject('PFM/XCUIElementTypeStaticText - Catatan Keuangan Tittle'), 10)

ranges = range.toString()

Mobile.tap(findTestObject('PFM/Laporan/XCUIElementTypeStaticText - Bulan ini'), 0)

switch(ranges) {
	case 'bulan ini':

		Mobile.tap(findTestObject('PFM/Laporan/pop up range/XCUIElementTypeStaticText - Bulan ini pop up'), 0)

		break;
	case 'bulan lalu':

		Mobile.tap(findTestObject('PFM/Laporan/pop up range/XCUIElementTypeStaticText - Bulan lalu pop up'), 0)

		break;
	case '3 bulan':

		Mobile.tap(findTestObject('PFM/Laporan/pop up range/XCUIElementTypeStaticText - 3 Bulan pop up'), 0)

		break;
}

Mobile.verifyElementExist(findTestObject('PFM/Laporan/XCUIElementTypeStaticText - Selisih'), 10)

Mobile.verifyElementExist(findTestObject('PFM/Laporan/XCUIElementTypeStaticText - Rp0'), 10)

Mobile.verifyElementExist(findTestObject('PFM/XCUIElementTypeStaticText - Belum ada catatan (1)'), 10)

CustomKeywords.'screenshot.capture.Screenshot'()